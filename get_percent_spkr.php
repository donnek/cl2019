<?php

/* 
*********************************************************************
Copyright Lois Catrin Donnelly, Kevin Donnelly 2019.
This file is part of the reproducible data for the CL2019 paper:
"Aspects of mutation in spoken conversational Welsh".

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script gathers information on percentages of features of interest per speaker.

include("includes/fns.php");
include("/opt/amscw/config.php");

// Total mutation rate.  Do not count English words as part of the total used to calculate this.
$entries[1]['target']=" mutation!=''";
$entries[1]['targetcol']="mutrate";
$entries[1]['incleng']="and langid!='eng'";

// Soft mutation.  Do not count English words as part of the total used to calculate this.
$entries[2]['target']=" mutation='SM'";
$entries[2]['targetcol']="sm";
$entries[2]['incleng']="and langid!='eng'";

// Nasal mutation.  Do not count English words as part of the total used to calculate this.
$entries[3]['target']=" mutation='NM'";
$entries[3]['targetcol']="nm";
$entries[3]['incleng']="and langid!='eng'";

// Aspirate mutation.  Do not count English words as part of the total used to calculate this.
$entries[4]['target']=" mutation='AM'";
$entries[4]['targetcol']="am";
$entries[4]['incleng']="and langid!='eng'";

// H-mutation.  Do not count English words as part of the total used to calculate this.
$entries[5]['target']=" mutation='H'";
$entries[5]['targetcol']="h";
$entries[5]['incleng']="and langid!='eng'";

// @cym (Welsh) words.
$entries[6]['target']=" langid='cym'";
$entries[6]['targetcol']='cym';
$entries[6]['incleng']="";

// @cym&eng (indeterminate) words.
$entries[7]['target']=" langid='cym&eng'";
$entries[7]['targetcol']='indet';
$entries[7]['incleng']="";

// @eng (English) words.
$entries[8]['target']=" langid='eng'";
$entries[8]['targetcol']='eng';
$entries[8]['incleng']="";

foreach ($entries as $entry)
{
    $target=$entry['target'];
    $targetcol=$entry['targetcol'];
    $incleng=$entry['incleng'];

    $result=query("select speaker, file from speakers order by speaker;");
    while ($row=pg_fetch_object($result))
    {
        $speaker=$row->speaker;
        $file=$row->file;
        echo $speaker.": ";
        
        $result1=query("select count(surface) from all_words where speaker='$speaker' and $target $incleng;");
        while ($row1=pg_fetch_object($result1))
        {
            $focus=$row1->count;
            //echo $focus;
        }
        
        $result2=query("select count(surface) from all_words where speaker='$speaker' $incleng;");
        while ($row2=pg_fetch_object($result2))
        {
            $total=$row2->count;
            //echo $words;
        }
        
        $percent=round($focus/$total*100, 2);
        
        echo $focus."/".$total.": ".$percent."\n";

        query("update siaradq set $targetcol=$percent where speaker='$speaker';");
    }
}

?>
