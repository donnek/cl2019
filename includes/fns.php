<?php

/* 
*********************************************************************
Copyright Lois Catrin Donnelly, Kevin Donnelly 2019.
This file is part of the reproducible data for the CL2019 paper:
"Aspects of mutation in spoken conversational Welsh"

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

function query($sql)
// simplify the query writing
// use this as: $result=query("select * from table");
{
    global $db_handle;
    return pg_query($db_handle,$sql);
}

function de_soft($text)
// Remove Welsh soft mutations so that the demutated word can be looked up.  Note that the order of these regex replacements is significant - letters in the regex must be placed before their occurrence as replacements.
{
    $text=preg_replace("/^g/", "c", $text);
    $text=preg_replace("/^G/", "C", $text);
    $text=preg_replace("/^r/", "rh", $text);
    $text=preg_replace("/^R/", "Rh", $text);
    $text=preg_replace("/^l([^l])/", "[gl]l$1", $text);
    $text=preg_replace("/^L([^l])/", "[GL]l$1", $text);
    $text=preg_replace("/^r([^h])/", "gr", $text);
    $text=preg_replace("/^R([^h])/", "Gr", $text);
    $text=preg_replace("/^([aeoiuwyïŵŷ])/", "g$1", $text);
    $text=preg_replace("/^([AEOIUWYÏŴŶ])/e", "G.strtolower('$1')", $text);  // we need to lowercase the now non-initial
    $text=preg_replace("/^f/", "[mb]", $text);
    $text=preg_replace("/^F/", "[MB]", $text);
    $text=preg_replace("/^b/", "p", $text);
    $text=preg_replace("/^B/", "P", $text);
    $text=preg_replace("/^d([^d])/", "t$1", $text);
    $text=preg_replace("/^D([^d])/", "T$1", $text);
    $text=preg_replace("/^dd/", "d", $text);
    $text=preg_replace("/^Dd/", "D", $text);
    return $text;
}

function de_nas($text)
// Remove Welsh nasal mutations so that the demutated word can be looked up.
{
    $text=preg_replace("/^ngh/", "c", $text);
    $text=preg_replace("/^Ngh/", "C", $text);
    $text=preg_replace("/^mh/", "p", $text);
    $text=preg_replace("/^Mh/", "P", $text);
    $text=preg_replace("/^nh/", "t", $text);
    $text=preg_replace("/^Nh/", "T", $text);
    $text=preg_replace("/^ng/", "g", $text);
    $text=preg_replace("/^Ng/", "G", $text);
    $text=preg_replace("/^m/", "b", $text);
    $text=preg_replace("/^M/", "B", $text);
    $text=preg_replace("/^n/", "d", $text);
    $text=preg_replace("/^N/", "D", $text);    
    return $text;
}

function de_asp($text)
// Remove Welsh aspirate mutations so that the demutated word can be looked up.
{
    $text=preg_replace("/^ch/", "c", $text);
    $text=preg_replace("/^Ch/", "C", $text);
    $text=preg_replace("/^ph/", "p", $text);
    $text=preg_replace("/^Ph/", "P", $text);
    $text=preg_replace("/^th/", "t", $text);
    $text=preg_replace("/^Th/", "T", $text);
    return $text;
}

function de_h($text)
// Remove Welsh h-mutation word-initially.
{
    $text=preg_replace("/^h/", "", $text);
    return $text;
}

?>
