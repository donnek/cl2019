This Git repo contains methodology and data for the following paper: 

> Lois Catrin Donnelly and Kevin Donnelly (2019).  Aspects of mutation in spoken conversational Welsh.  Corpus Linguistics 2019, Cardiff, 22-26 July 2019.

The paper is available [here](docs/CL2019_Aspects_Mutation_Welsh.pdf).

The presentation, which includes more detail (e.g. tables and graphs), is available [here](docs/cl2019.pdf).  The individual graphs are in *docs/revgraphs*.

The data collection and handling is described [here](docs/methodology_data.md).

The code for the statistics and graphs is described [here](docs/methodology_stats.md).

All the material is made available under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and [Affero GPLv3](https://www.gnu.org/licenses/agpl-3.0.html) licences.  The text of each licence is provided in this repository.
