<?php

/* 
*********************************************************************
Copyright Lois Catrin Donnelly, Kevin Donnelly 2019.
This file is part of the reproducible data for the CL2019 paper:
"Aspects of mutation in spoken conversational Welsh".

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script demutates the initial letter of each word.

include("includes/fns.php");
include("/opt/amscw/config.php");

$result=query("select mutation from all_mutated group by mutation order by mutation;");
while ($row=pg_fetch_object($result))
{
    $mutation=$row->mutation;
    
    $result1=query("select initlet from all_mutated where mutation='$mutation' group by initlet order by initlet;");
    while ($row1=pg_fetch_object($result1))
    {
	$initlet=$row1->initlet;
	echo $mutation.": ".$initlet." > ";
	
	if ($mutation=='SM')
	{
	    $demutlet=de_soft($initlet);
	}
	elseif ($mutation=='NM')
	{
	    $demutlet=de_nas($initlet);
	}
	elseif ($mutation=='AM')
	{
	    $demutlet=de_asp($initlet);
	}
	elseif ($mutation=='H')
	{
	    $demutlet=de_h($initlet);
	}
	
	echo $demutlet."\n";

	$result2=query("update all_mutated set demutlet='$demutlet' where initlet='$initlet' and mutation='$mutation';");
    }
}

?>