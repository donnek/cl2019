# Methodology: statistics

The statistical analysis uses [R](https://www.r-project.org).  You may wish to use a graphical interface like [RStudio](https://www.rstudio.com/).   The following code could be run from a script, but here it is presented as if each test or graph is being investigated interactively, so there is some repetition in the code.


## Workplace setup

~~~r
rm(list=ls())  # Clear your workspace to make a clean start.  Note: this will clear everything!

library(RPostgreSQL)
library(sqldf)
options(sqldf.RPostgreSQL.user = "kevin", sqldf.RPostgreSQL.password = "kevindbs", sqldf.RPostgreSQL.dbname = "amscw", sqldf.RPostgreSQL.host = "localhost", sqldf.RPostgreSQL.port = 5432)
library(ggplot2)
theme_set(theme_bw())  # Set the default theme for ggplot.
library(reshape2)
library(ggcorrplot)
library(effects)
~~~


## Scatterplots

### cym+age

~~~r
thisplot<-sqldf("select speaker, age, cym, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=age, y=cym)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(cym<70, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(cym>93, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="lm", se=F, col="gray") +
  xlim(c(10, 90)) + 
  ylim(c(55, 100)) + 
  labs(title="% Welsh words used, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Welsh words used", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1,0), 
        legend.position=c(0.95, 0.05),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![cym+age](revgraphs/cym+age.png  "cym+age")

### indet+age

~~~r
thisplot<-sqldf("select speaker, age, indet, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;") # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=age, y=indet)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(indet<5, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(indet>22, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="lm", se=F, col="gray") +
  xlim(c(10, 90)) + 
  ylim(c(0, 31)) + 
  labs(title="% Indeterminate words used, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Indeterminate words used", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.95),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![indet+age](revgraphs/indet+age.png  "indet+age")

### eng+age

~~~r
thisplot<-sqldf("select speaker, age, eng, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;") # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=age, y=eng)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(eng<0, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(eng>10, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="loess", se=F, col="gray") +
  xlim(c(10, 90)) + 
  ylim(c(0, 25)) + 
  labs(title="% English words used, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% English words used", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.95),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![eng+age](revgraphs/eng+age.png  "eng+age")

### mutrate+age

~~~r
thisplot<-sqldf("select speaker, age, mutrate, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=age, y=mutrate)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(mutrate<8, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(mutrate>15, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="lm", se=F, col="gray") +
  xlim(c(10, 90)) + 
  ylim(c(0, 20)) + 
  labs(title="% Mutation rate, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.70),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![mutrate+age](revgraphs/mutrate+age.png  "mutrate+age")

Test the significance of the correlation between mutation rate and age:
~~~r
cor.test(thisplot$age, thisplot$mutrate)
~~~

~~~r
t = -3.3394, df = 146, p-value = 0.001066
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.4101022 -0.1097578
sample estimates:
       cor 
-0.2663843
~~~

The correlation is significant, and likely to be real because it does not cross zero.

### Mutation types by age

~~~r
thisplot<-sqldf("select speaker, age, sm, nm, am, h, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})
~~~

**sm+age**

~~~r
ggplot(thisplot, aes(x=age, y=sm)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_smooth(method="lm", se=F, col="gray") +
  geom_text(aes(label=ifelse(sm<7.5, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(sm>14.5, as.character(speaker),'')), hjust = -0.2) +
  xlim(c(10, 90)) + 
  ylim(c(3, 16)) + 
  labs(title="% Soft mutation, by age of speaker",
       y="% Soft mutation", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.75),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![sm+age](revgraphs/sm+age.png  "sm+age")

~~~r
cor.test(thisplot$age, thisplot$sm)
~~~

~~~r
t = -3.8907, df = 146, p-value = 0.0001514
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.4457999 -0.1527099
sample estimates:
       cor 
-0.3065019
~~~

**nm+age**

~~~r
ggplot(thisplot, aes(x=age, y=nm)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_smooth(method="lm", se=F, col="gray") +
  geom_text(aes(label=ifelse(nm>0.4, as.character(speaker),'')), hjust = -0.2) +
  xlim(c(10, 90)) + 
  ylim(c(0, 0.76)) + 
  labs(title="% Nasal mutation rate, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.95),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![nm+age](revgraphs/nm+age.png  "nm+age")

~~~r
cor.test(thisplot$age, thisplot$nm)
~~~

~~~r
t = 4.478, df = 146, p-value = 1.509e-05
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.1972175 0.4818325
sample estimates:
      cor 
0.3475039 
~~~

**am+age**

~~~r
ggplot(thisplot, aes(x=age, y=am)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_smooth(method="lm", se=F, col="gray") +
  geom_text(aes(label=ifelse(am>0.2, as.character(speaker),'')), hjust = -0.2) +
  xlim(c(10, 90)) + 
  ylim(c(0, 0.31)) + 
  labs(title="% Mutation rate, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.95),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![am+age](revgraphs/am+age.png  "am+age")

~~~r
cor.test(thisplot$age, thisplot$am)
~~~

~~~r
t = 6.045, df = 146, p-value = 1.19e-08
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.3083332 0.5677760
sample estimates:
     cor 
0.447419 
~~~

**h+age**

~~~r
ggplot(thisplot, aes(x=age, y=h)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_smooth(method="lm", se=F, col="gray") +
  geom_text(aes(label=ifelse(h>0.07, as.character(speaker),'')), hjust = -0.2) +
  xlim(c(10, 90)) + 
  ylim(c(0, 0.16)) + 
  labs(title="% H prothesis rate, by age of speaker",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="Age",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.75),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![h+age](revgraphs/h+age.png  "h+age")

~~~r
cor.test(thisplot$age, thisplot$h)
~~~

~~~r
t = 1.2379, df = 146, p-value = 0.2177
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.06041995  0.25900229
sample estimates:
      cor 
0.1019174 
~~~

### mutrate+cym

~~~r
thisplot<-sqldf("select speaker, cym, mutrate, lang1 from siaradq where lang1<4 and lang1 is not null and speaker!='GRG' order by rno;")  # remove Dutch speakers, missing values, and one outlier from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=cym, y=mutrate)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(mutrate<8, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(mutrate>15, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(cym<72, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="lm", se=F, col="gray") +
  xlim(c(55, 95)) + 
  ylim(c(4, 16)) + 
  labs(title="% Mutation rate, by % Welsh words",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="% Welsh words",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.25),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![mutrate+cym](revgraphs/mutrate+cym.png  "mutrate+cym")

~~~r
cor.test(thisplot$mutrate, thisplot$cym)
~~~

~~~r
t = 5.825, df = 146, p-value = 3.503e-08
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.2934742 0.5566017
sample estimates:
      cor 
0.4342559 
~~~

### mutrate+indet

~~~r
thisplot<-sqldf("select speaker, indet, mutrate, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=indet, y=mutrate)) + 
  geom_point(aes(col=factor(lang1)), size = 2) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_text(aes(label=ifelse(mutrate<8, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(mutrate>15, as.character(speaker),'')), hjust = -0.2) +
  geom_smooth(method="lm", se=F, col="gray") +
  xlim(c(3, 28)) + 
  ylim(c(3, 17)) + 
  labs(title="% Mutation rate, by % indet words",
       subtitle="Colour-coded by first language", 
       y="% Mutation rate", 
       x="% Indeterminate words",
       caption = "Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.05, 0.30),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![mutrate+indet](revgraphs/mutrate+indet.png  "mutrate+indet")

~~~r
cor.test(thisplot$mutrate, thisplot$indet)
~~~

~~~r
t = -6.1949, df = 146, p-value = 5.629e-09
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.5752309 -0.3183171
sample estimates:
       cor 
-0.4562297 
~~~

## Density plot

### sex+age

~~~r
thisplot<-sqldf("select age, sex from siaradq;")

thisplot<-within(thisplot, {sex<-factor(sex, labels=c('Female','Male'))})

ggplot(thisplot, aes(age)) +
  scale_fill_manual(values=c("red", "blue")) +
  geom_density(aes(fill=factor(sex)), alpha=0.5) +
  labs(title="Sex, by age of speaker",
       y="Numbers", 
       x="Age",
       caption="Source: Donnelly and Donnelly 2019",
       fill="Gender")
~~~

![sex+age](revgraphs/sex+age.png  "sex+age")


## Histogram

### mutation_rate

~~~r
thisplot<-sqldf("select mutrate from siaradq;")

ggplot(thisplot, aes(mutrate)) + 
  geom_histogram(aes(y=..count..),
                 bins=30,
                 fill="blue",
                 alpha =0.5) +
  geom_vline(aes(xintercept=mean(mutrate)),
             color="red", linetype="dashed", size=1, alpha=0.5) +
  labs(title="Mutation rate", 
       x="% Mutation rate", 
       y="Number",
       caption="Source: Donnelly and Donnelly 2019")
~~~

![mutation_rate](revgraphs/mutation_rate.png  "mutation_rate")


## Boxplots

The setting "varwidth=T" adjusts the width of the box to be proportional to the number of observations it contains.

### educ+age

~~~r
thisplot<-sqldf("select speaker, age, educ, lang1 from siaradq where lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {educ<-factor(educ, labels=c('none','GCSE','A-level','graduate','postgrad'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=educ, y=age)) +
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20, h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  coord_flip() +
  labs(title="Age grouped by education",
       subtitle="Colour-coded by first language",
       x="Education",
       y="Age",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.70),  
        legend.background = element_blank(),
        legend.key = element_blank())      
~~~

![educ+age](revgraphs/educ+age.png  "educ+age")

### mutrate+sex

~~~r
thisplot<-sqldf("select speaker, sex, mutrate, lang1 from siaradq where lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot <- within(thisplot, {sex <- factor(sex, labels=c('Female','Male'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=sex, y=mutrate)) + 
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue", "orange", "brown")) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  labs(title="Mutation rate grouped by gender",
       subtitle="Colour-coded by first language",
       x="Gender",
       y="Mutation rate",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.55, 0.10),  
        legend.background = element_blank(),
        legend.key = element_blank(),
        axis.text.x=element_text(size=20),
        axis.title.x=element_blank())
~~~

![mutrate+sex](revgraphs/mutrate+sex.png  "mutrate+sex")

### mutrate+educ

~~~r
thisplot<-sqldf("select speaker, educ, mutrate, lang1 from siaradq where lang1<4 and lang1 is not null order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {educ<-factor(educ, labels=c('none','GCSE','A-level','graduate','postgrad'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=educ, y=mutrate)) + 
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue", "orange", "brown")) +
  geom_text(aes(label=ifelse(mutrate<8, as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(mutrate>14.5, as.character(speaker),'')), hjust = -0.2) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  labs(title="Mutation rate grouped by education", 
       x="Education",
       y="Mutation rate",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.10),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![mutrate+educ](revgraphs/mutrate+educ.png  "mutrate+educ")

### separate+age

~~~r
thisplot<-sqldf("select speaker, age, separate, lang1 from siaradq where lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot <- within(thisplot, {separate <- factor(separate, labels=c('strongly disagree','disagree','neither','agree', 'strongly agree'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=separate, y=age)) + 
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  coord_flip() +
  labs(title="Age grouped by 'I keep separate'",
       subtitle="Colour-coded by first language",
       x="'I keep both languages separate'",
       y="Age",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(0, 1), 
        legend.position=c(0.00, 1.00),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![separate+age](revgraphs/separate+age.png  "separate+age")

### network+age

~~~r
thisplot<-sqldf("select age, network, lang1 from siaradq where network is not null and lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot <- within(thisplot, {network <- factor(network, labels=c('mainly Welsh','both','mainly English'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=network, y=age)) +
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue")) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) +
  coord_flip() +
  labs(title="Language of social network, grouped by age",
       x="Language",
       y="Age",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.90, 0.75),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![network+age](revgraphs/network+age.png  "network+age")

### cym+welsince

~~~r
thisplot<-sqldf("select speaker, welsince, cym, lang1 from siaradq where welsince is not null and lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {welsince<-factor(welsince, labels=c('2yo','4yo','primary','secondary','adult'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=welsince, y=cym)) +
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue", "orange", "brown")) +
  ylim(c(55, 100)) + 
  geom_text(aes(label=ifelse(cym<70, as.character(speaker),'')), hjust = -0.2) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  labs(title="% Welsh words grouped by age of Welsh acquisition",
       subtitle="Colour-coded by first language",
       x="Age of Welsh acquisition",
       y="% Welsh words",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language")
~~~

![cym+welsince](revgraphs/cym+welsince.png  "cym+welsince")

### mutrate+welsince

~~~r
thisplot<-sqldf("select speaker, welsince, mutrate, lang1 from siaradq where welsince is not null and lang1 is not null and lang1!=4 order by rno;")  # remove Dutch speakers and missing values from "lang1"

thisplot<-within(thisplot, {welsince<-factor(welsince, labels=c('2yo','4yo','primary','secondary','adult'))})

thisplot<-within(thisplot, {lang1<-factor(lang1, labels=c('Welsh','both','English'))})

ggplot(thisplot, aes(x=welsince, y=mutrate)) + 
  geom_jitter(aes(col=factor(lang1)), position=position_jitter(width=0.20,h=0.20), alpha=1) +
  scale_colour_manual(values=c("red", "green", "blue", "orange", "brown")) +
  geom_text(aes(label=ifelse(welsince=="4yo", as.character(speaker),'')), hjust = -0.2) +
  geom_text(aes(label=ifelse(welsince=="secondary", as.character(speaker),'')), hjust = -0.2) +
  geom_boxplot(varwidth=T, fill="red", outlier.size=0, alpha=0.2) + 
  labs(title="Mutation rate grouped by age of Welsh acquisition",
       subtitle="Colour-coded by first language",
       x="Age of Welsh acquisition",
       y="Mutation rate",
       caption="Source: Donnelly and Donnelly 2019",
       color="First language") +
  theme(legend.title = element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.85, 0.70),  
        legend.background = element_blank(),
        legend.key = element_blank())
~~~

![mutrate+welsince](revgraphs/mutrate+welsince.png  "mutrate+welsince")


## Venn diagram

To get the relevant figures, use variants of:
~~~sql
select count(*) from siaradq where sm!=0 and nm!=0 and am!=0 and h!=0;
~~~

![mutation_groups](revgraphs/mutation_groups.png  "mutation_groups")

          
## Barcharts

### Mutateable letters, raw numbers

~~~r
letmut<-sqldf("select letter, 
    count(letter) as total,
    count(mutation) as mutated,
    count(letter)-count(mutation) as nonmutated,
    100*count(mutation)/count(letter) as mutperc,
    100*(count(letter)-count(mutation))/count(letter) as nomutperc
  from all_words
  where langid!='eng'
  and letter in ('b', 'c', 'd', 'g', 'll','m', 'p', 'rh', 't')
  group by letter
  order by total desc;")

letord<-sqldf("select letter from all_words where langid!='eng' and letter in ('b', 'c', 'd', 'g', 'll','m', 'p', 'rh', 't') group by letter order by count(letter) desc;")

lmsubset<-subset(letmut, select=c(1,3,4))  # Select the letter, and the figures for mutated and non-mutated.

lmlong<-melt(lmsubset, id.vars=1)  # Change data from wide format to long format.

lmlong$letter<-factor(lmlong$letter, levels=factor(letord$letter))  # Apply the order of the letters in letord to those in lmlong.

ggplot(lmlong, aes(x=letter, y=value, fill=rev(variable))) +
  geom_bar(stat="identity") + 
  labs(title="Mutateable letters and their mutation",
       y="Number", 
       x="Letter",
       caption="Source: Donnelly and Donnelly 2019") +
  scale_fill_manual(values=c(alpha("blue", 0.5),
                             alpha("red", 0.5)), 
                    labels=c("Non-mutated", "Mutated"), 
                    name=element_blank()) +
  theme(legend.title=element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.75),
        axis.text.x=element_text(size=20),
        axis.title.x=element_blank())
~~~

![letter_mutation](revgraphs/letter_mutation.png  "letter_mutation")

### Mutateable letters, percentages

~~~r
letmut<-sqldf("select letter, 
  count(letter) as total,
  count(mutation) as mutated,
  count(letter)-count(mutation) as nonmutated,
  100*count(mutation)/count(letter) as mutperc,
  100*(count(letter)-count(mutation))/count(letter) as nomutperc
  from all_words
  where langid!='eng'
  and letter in ('b', 'c', 'd', 'g', 'll','m', 'p', 'rh', 't')
  group by letter
  order by mutperc desc;")

letord<-sqldf("select letter from all_words where langid!='eng' and letter in ('b', 'c', 'd', 'g', 'll','m', 'p', 'rh', 't') group by letter order by (100*count(mutation)/count(letter)) desc;")

lmsubset<-subset(letmut, select=c(1,5,6))  # Select the letter, and the percentages for mutated and non-mutated.

lmlong<-melt(lmsubset, id.vars=1)  # Change data from wide format to long format.

lmlong$letter<-factor(lmlong$letter, levels=factor(letord$letter))  # Apply the order of the letters in letord to those in lmlong.

ggplot(lmlong, aes(x=letter, y=value, fill=rev(variable))) +
  geom_bar(stat="identity") + 
  labs(title=" Percentage mutation of mutateable letters",
       y="Percentage (sums to 100%)", 
       x="Letter",
       caption="Source: Donnelly and Donnelly 2019") +
  scale_fill_manual(values=c(alpha("blue", 0.5), 
                             alpha("red", 0.5)),
                    labels=c("Non-mutated", "Mutated"),
                    name=element_blank()) +
  theme(legend.title=element_text(size=12),
        legend.justification=c(1, 0), 
        legend.position=c(0.95, 0.75),
        axis.text.x=element_text(size=20),
        axis.title.x=element_blank())
~~~

![letter_mutation_percent](revgraphs/letter_mutation_percent.png  "letter_mutation_percent")


## Correlogram

~~~r
siaradnum=sqldf("select age, cym, educ, indet, mutrate, network, separate,  welsince from siaradq;")

siaradcor=round(cor(siaradnum, use="pairwise.complete.obs", method="pearson"), 1)  #Although many of the sociolinguistic variables are categorical, they are also ordinal, so use of Pearson’s r is justified.

ggcorrplot(siaradcor, 
           hc.order=TRUE, # tries to cluster results together hierarchically
           type="lower",
           lab=TRUE, 
           lab_size=4, 
           method="square",
           colors=c("blue", "white", "red"), 
           title="Correlogram of salient speaker data",
           legend.title="p-value",
           ggtheme=theme_minimal)
~~~

![correlation](revgraphs/correlation.png  "correlation")


## Regression

### Set up data

~~~r
siareg<-sqldf("select * from siaradq order by rno")

siareg<-within(siareg, {welsince<-factor(welsince, labels=c('2yo','4yo','primary','secondary','adult'))})

siareg<-within(siareg, {educ<-factor(educ, labels=c('none','GCSE','A-level','graduate','postgrad'))})

siareg<-within(siareg, {engsince<-factor(engsince, labels=c('2yo','4yo','primary','secondary','adult'))})

siareg<-within(siareg, {sex<-factor(sex, labels=c('F','M'))})

siareg <- within(siareg, {separate <- factor(separate, labels=c('strongly disagree','disagree','neither','agree', 'strongly agree'))})

siareg <- within(siareg, {network <- factor(network, labels=c('mainly Welsh','both','mainly English'))})

siareg <- within(siareg, {sex <- factor(sex, labels=c('Female','Male'))})

siareg <- within(siareg, {lang2 <- factor(lang2, labels=c('simultaneous', 'L2 by 4yo', 'L2 in primary', 'L2 in secondary', 'L2 as adult'))})

siareg<-within(siareg, {lang1<-factor(lang1, labels=c('Welsh','both','English', 'Dutch'))})
~~~

### Ten variables

This model accounts for 50% of the variation.

~~~r
lm_all<-lm(mutrate ~ sex + age + educ + welsince + engsince + separate + network + cym + indet + eng, data=siareg, na.action=na.exclude)

summary(lm_all)

Residuals:
    Min      1Q  Median      3Q     Max 
-3.6870 -0.8395  0.0649  0.8129  3.5542 

Coefficients:
                         Estimate Std. Error t value Pr(>|t|)
(Intercept)            -49.692975  53.442240  -0.930  0.35426
sexMale                  0.068547   0.253027   0.271  0.78691
age                     -0.041186   0.007209  -5.713 7.79e-08 ***
educGCSE                 0.177254   0.533502   0.332  0.74026
educA-level             -0.211330   0.430303  -0.491  0.62421
educgraduate             0.206074   0.411730   0.501  0.61760
educpostgrad             0.545303   0.536390   1.017  0.31132
welsince4yo              0.664864   0.937558   0.709  0.47957
welsinceprimary         -0.656307   0.582761  -1.126  0.26225
welsincesecondary       -2.285046   1.495448  -1.528  0.12906
welsinceadult           -2.244575   0.677559  -3.313  0.00121 ***
engsince4yo             -0.011485   0.355942  -0.032  0.97431
engsinceprimary          0.335294   0.367580   0.912  0.36345
engsincesecondary        0.470127   0.545012   0.863  0.39002
engsinceadult            1.017165   1.522564   0.668  0.50534
separatedisagree        -0.505163   0.437637  -1.154  0.25060
separateneither          0.304740   0.445859   0.683  0.49557
separateagree           -0.544335   0.470817  -1.156  0.24984
separatestrongly agree  -0.202229   0.515343  -0.392  0.69542
networkboth             -0.683623   1.386923  -0.493  0.62295
networkmainly English   -0.325143   0.447733  -0.726  0.46909
cym                      0.654504   0.537046   1.219  0.22527
indet                    0.470729   0.536370   0.878  0.38185
eng                      0.603976   0.540171   1.118  0.26568

Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.419 on 124 degrees of freedom
  (3 observations deleted due to missingness)
Multiple R-squared:  0.5768,	Adjusted R-squared:  0.4983 
F-statistic: 7.347 on 23 and 124 DF,  p-value: 3.453e-14

AIC(lm_all)

547.4529

plot(allEffects(lm_all))
~~~

![effects_all](revgraphs/effects_all.png  "effects_all")

### Three variables

After exploring various options, a model using only the variables *welsince* (age of learning Welsh), *indet* (percentage of indeterminate words used) and *age* accounts for 47% of the variance in mutation rate (compared to 50% for the 10-variable model), and is likely to be more robust.  Fit is almost as good as the 10-variable model (AIC of 552 compared to 547).

~~~r
lm_lim<-lm(mutrate ~ welsince + indet + age, data=siareg, na.action=na.exclude)

summary(lm_lim)

Residuals:
    Min      1Q  Median      3Q     Max 
-5.2949 -0.8332  0.0233  0.7577  4.5429 

Coefficients:
                   Estimate Std. Error t value Pr(>|t|)    
(Intercept)       15.390000   0.521159  29.530  < 2e-16 ***
welsince4yo        0.715900   0.869829   0.823   0.4119
welsinceprimary   -1.043319   0.488277  -2.137   0.0343 *
welsincesecondary -2.115953   1.493173  -1.417   0.1586
welsinceadult     -2.797512   0.583299  -4.796 4.02e-06 ***
indet             -0.209119   0.030647  -6.823 2.33e-10 ***
age               -0.030306   0.006139  -4.936 2.19e-06 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.479 on 143 degrees of freedom
  (1 observation deleted due to missingness)
Multiple R-squared:  0.4882,	Adjusted R-squared:  0.4667 
F-statistic: 22.73 on 6 and 143 DF,  p-value: < 2.2e-16

AIC(lm_lim)

551.8716

plot(allEffects(lm_lim))
~~~

![effects](revgraphs/effects.png  "effects")

### Model predictions against the data

~~~r
df_all=data.frame(mutrate=siareg$mutrate, lang1=siareg$lang1, prediction=predict(lm_all))
ggplot(df_all, aes(x=mutrate, y=prediction)) + 
  geom_point(aes(col=factor(lang1)), size = 2, alpha=1, show.legend=FALSE) + 
  scale_colour_manual(values=c("red", "green", "blue", "white")) +
  geom_line(aes(y=mutrate), colour="gray") + 
  labs(title="The regression model's predictions against the actual data (all variables)",
       y="Prediction", 
       x="Mutation rate",
       caption = "Source: Donnelly and Donnelly 2019")
~~~


![prediction_all+mutrate](revgraphs/prediction_all+mutrate.png  "prediction_all+mutrate")

~~~r
df_lim=data.frame(mutrate=siareg$mutrate, lang1=siareg$lang1, prediction=predict(lm_lim))
ggplot(df_lim, aes(x=mutrate, y=prediction)) + 
  geom_point(aes(col=factor(lang1)), size = 2, alpha=1, show.legend=FALSE) + 
  scale_colour_manual(values=c("red", "green", "blue", "white")) +
  geom_line(aes(y=mutrate), colour="gray") + 
  labs(title="The regression model's predictions against the actual data (age, indet, welsince)",
       y="Prediction", 
       x="Mutation rate",
       caption = "Source: Donnelly and Donnelly 2019")
~~~

![prediction+mutrate](revgraphs/prediction+mutrate.png  "prediction+mutrate")
