# Methodology: data preparation

## Load the autoglosser tables

The relevant tables for the data in the paper are available as a PostgreSQL dbdump in *dbdumps/amscw.sql*.  This includes the source autoglosser tables as well as the generated tables mentioned below. 

If you want to load the autoglosser tables from scratch, they are in *siarad_words* and *siarad_utterances*, and you can load them using something like *sh_load_corpus*.

The default connection settings are: 

- username: *kevin*
- password: *kevindbs*
- database: *amscw*

You can either retain these when you set up your own database, or alternatively you can change them (in *amscw/config.php* for all the PHP scripts, and directly in the file for all Bash scripts) to suit your own database.

To load the dump into your PostgreSQL instance:
~~~bash
psql -U kevin -d amscw < dbdumps/amscw.sql
~~~

Copy the *amscw* directory to */opt* to match the setting in the PHP scripts:
~~~bash
sudo cp -R amscw/ /opt/
~~~

The SQL (database) commands below can be run from a *psql* terminal, accessed by:
~~~bash
psql -U kevin -d amscw
~~~
and exited by:
~~~sql
\q
~~~

## Get all speakers

Run *sh_get_speakers* to get a list of speakers from the **_cgwords** tables (*siarad_speakers.tsv*), and use that to create the **speakers** table.

~~~sql
create table speakers (
    id serial primary key,
    file character varying(50),
    speaker character varying(5)
);
~~~
~~~bash
\copy amush (file, speaker) from 'siarad_speakers.tsv' delimiter E'\t' csv header;
~~~

## Amalgamate word data

Create a table to hold all the word data:
~~~sql
create table all_words (
    id serial primary key,
    file character varying(50),
    speaker character varying(10),
    surface character varying(100),
    auto character varying(250),
    langid character varying(20),
    mutation character varying(5),
    fixed character varying(250),
    initlet character varying(5),
    present character varying(10),
    demutlet character varying(10),
    letter character varying(10),
    utterance_id integer
);
~~~

Run *sh_combine_words* to fill the **all_words** table with the data from all the ** _cgwords** tables.

Check total:
~~~sql
select count(*) from all_words;
~~~
525,768

Check total, omitting punctuation entries (marked 999):
~~~sql
select count(*) from all_words where langid!='999';
~~~
447,507

>NOTE: The **all_words** table in the dump has only 446,083 non-punctuation entries, because some speakers were barred (see below):
>`select count(*) from binkie where langid!='999' and speaker not in ('ART', 'CYW', 'RES', 'CAI', 'CIG', 'SAN', 'HAF', 'MAD');`
>446,083

## Collect mutations

Extract words marked as mutated into a new table, **all_mutated**:
~~~sql
create table all_mutated as select * from all_words where auto~'\+(SM|AM|NM|H)$';
~~~

53,650

The id for each of these entries is the same, by definition, as the id for the same item in the **all_words** table, and we use this later when re-integrating corrected mutation data.

Add a new column "present" to mark that mutation is present:
~~~sql
alter table all_mutated add column present character varying (10);
~~~

This column will hold a marker to confirm that the mutation is indeed a real mutation, and not  a mischaracterisation by the autoglosser.  Manually inspect the list and mark these items by putting *y* in the "present" column.

Some items are not completely disambiguated: 
~~~sql
select count(*) from all_mutated where auto~'\[or\]';
~~~

Add a new column "fixed":
~~~sql
alter table all_mutated add column fixed character varying (250);
~~~
and copy the auto column into it:
~~~sql
update all_mutated set fixed=auto;
~~~

Collect the non-disambiguated items:
~~~sql
select count(*), surface, fixed, present from all_mutated where fixed~'\[or\]' group by surface, fixed, present order by surface, fixed, present;
~~~
and manually inspect them to resolve the ambiguity, marking *y* in the "present" column when there is a mutation.

Delete all items in **all_mutated** which are not mutated:
~~~sql
delete from all_mutated where present!='y';
~~~

Add a new column "mutation" to hold the type of mutation:
~~~sql
alter table all_mutated add column mutation character varying (5);
~~~
and insert the type of mutation:
~~~sql
update all_mutated set mutation='SM' where fixed~'\+SM';

update all_mutated set mutation='NM' where fixed~'\+NM';

update all_mutated set mutation='AM' where fixed~'\+AM';

update all_mutated set mutation='H' where fixed~'\+H';
~~~

Remove instances of *hefo* (not really a H mutation of efo).

Total mutated items left after the above cleaning:
~~~sql
select count(*) from all_mutated;
~~~
47,683

>NOTE: This is lower than the figure of 48,963 used in the paper because further manual inspection of **all_words** after **all_mutated** was copied over showed some mutated words which were not marked as such by the autoglosser.

## Letter distribution

Get the initial letters for each surface word.  We use 4 letters because this allows us to remove *h* from sequences like *ngh* and still get the following vowel, and also to handle initial accented letters (*ŵ*) correctly.  The output will be stripped down later to get the initial letter of the word.

Add a column to hold the initial letters:
~~~sql
alter table all_mutated add column initlet character varying (5);
~~~
and copy the first 4 letters of each item into it:
~~~sql
update all_mutated set initlet=substring(surface,1,4);
~~~

Inspect the number of each sequence, 1,187 in total:
~~~sql
select count(*), initlet from all_mutated group by initlet order by initlet;
~~~

Add a column to hold the demutation letter sequences:
~~~sql
alter table all_mutated add column demutlet character varying (10);
~~~

Demutate the initial letter:
~~~bash
php demut_initlet.php
~~~

Note that since it is impossible to know whether *F* should demutate to *M* or to *B*, and whether *L* should demutate to *GL* or *LL*, the demutation of *F* and *L* resolves to *[GL]* and *[MB]*, leaving both options.  This will be corrected at the next step.

Set up a new column to hold the first letter.
~~~sql
alter table all_mutated add column letter character varying (10);
~~~
and copy "demutlet" into it:
~~~sql
update all_mutated set letter=demutlet;
~~~

Manually correct *[gl], [GL, [mb]* and *[MB]*  to the correct initial letter: *g, l, m* or *b*.

Trim the entries in the "letter" column to the first letter, except for digraphs *ll* and *rh*. The length varies from 1 (*[gl]* and *[mb]* as manually fixed above) to 5 (e.g. *yrfa* demutated to *gyrfa*).  Start at length 5 and drop all but the first letter.  Then do the same for length 4 and length 3.
~~~sql
update all_mutated set letter=regexp_replace(letter, E'.$', '', 'g') where length(letter)=5;

update all_mutated set letter=regexp_replace(letter, E'.$', '', 'g') where length(letter)=4;

update all_mutated set letter=regexp_replace(letter, E'.$', '', 'g') where length(letter)=3;
~~~
For length 2 we need to protect the digraphs *ll* and *rh*: 
~~~sql
update all_mutated set letter=regexp_replace(letter, E'.$', '', 'g') where length(letter)=2 and letter!='ll' and letter!='Ll' and letter!='rh' and letter!='Rh';
~~~

Lowercase capitals in the "letter" column:
~~~sql
update all_mutated set letter=lower(letter);
~~~

Review the results for each letter using:
~~~sql
select count(*), surface, fixed, letter from all_mutated where letter='X' group by surface, fixed, letter order by surface, fixed, letter;
~~~
replacing X with the relevant graph or digraph.  The vowels are all instances of H-mutation.

## Copy mutation data into all_words

Get the initial letter of the non-mutated words in the **all_words** table.  We do this for all words, mutated and non-mutated, because below we will overwrite the incorrect results for the former with the entries from the **all_mutated** table.

Copy the first two letters of the word to the letter column:
~~~sql
update all_words set letter=substring(surface,1,2);
~~~

Trim the entries to the first letter, apart from digraphs:
~~~sql
update all_words set letter=regexp_replace(letter, E'.$', '', 'g') where length(letter)=2 and letter!='ll' and letter!='Ll'  and letter!='ch' and letter!='Ch' and letter!='ff' and letter!='Ff' and letter!='rh' and letter!='Rh';
~~~

Decapitalise:
~~~sql
update all_words set letter=lower(letter);
~~~

Review total numbers:
~~~sql
select count(*), letter from all_words group by letter order by length(letter), count;
~~~

Review individual letters (replace X with the relevant graph or digraph):
~~~sql
select count(*), surface, letter from all_words where letter='X' group by surface, letter order by surface, letter;
~~~

Delete 78,261 punctuation entries:
~~~sql
delete from all_words where langid='999';
~~~

Copy the corrected mutation data into the **all_words** table:
~~~sql
update all_words w set fixed=m.fixed, mutation=m.mutation, letter=m.letter from all_mutated m where m.id=w.id;
~~~

Review individual letters (replace X with the relevant graph or digraph):
~~~sql
select count(*), surface, langid, mutation, letter from all_words where letter='X' group by surface, langid, mutation, letter order by count desc, surface, langid, mutation, letter;
~~~

Manually inspect the data again to make further corrections as appropriate.

## Remove input from some speakers

Remove words spoken by the researcher:
~~~sql
delete from all_words where speaker='RES';
~~~

Remove words from 7 speakers (1,244 words in total) who gave limited input to the conversations:

- **adults**:
    - ART lloyd1 (589)
    - CIG fusser7 (22)
    - MAD fusser28 (44)
    - SAN lloyd1 (153)
- **children**:
    - CAI fusser18 (40)
    - CYW fusser14 (362)
    - HAF fusser18 (34)

Fix 6 incorrect *cym&eng* langid tags (5 *cym&eng1*, 1 *cym&eng?*).  52 other incorrect langid tags have been left as is. 

## Final dataset

After the above cleaning, the results are as follows:

| type | number |
| --- | --- |
| total words | 446,083 |
| total non-English words | 429,656 |
| total mutated words | 49,018 |
| total non-English mutated words | 48,963

~~~sql
select count(*) from all_words;

select count(*) from all_words where langid!='eng';

select count(*) from all_words where mutation!='';

select count(*) from all_words where langid!='eng' and mutation!='';
~~~

Note that the mutated total is 55 more than the non-English mutated total, accounted for by the 55 mutated English words in *english_mutated.tsv*, all but 2 of which showed soft mutation
~~~sql
select count(*) from all_words where langid='eng' and mutation!='';

select surface, mutation, count(*) from all_words where langid='eng' and mutation!='' group by surface, mutation order by surface, mutation;

select surface, mutation, count(*) from all_words where langid='eng' and mutation is not null group by surface, mutation order by mutation desc, surface;
~~~

Words classified by language are in *langid_numbers.pdf* and *langid_numbers.ods*.

| type | numbers | percent |
| --- | --- | --- |
| indeterminate % of total non-English words | 56270 / 429656 | 13.1% |
| mutation of indeterminate words | 718 / 48963 | 1.5% |
| mutation of Welsh words only | 48230 / 372763 | 12.9% |

##Context of mutated words

The inclusion of the *utterance_id* allows the syntax of utterances containing the mutated word to be examined to investigate the contexts in which that mutation occurs.  This was outside the scope of the present study, so it was not pursued.  However, some materials are provided here for such a study.

Create a table to hold all the utterance data:
~~~sql
create table all_utterances (
    id serial primary key,
    file character varying(50),
    speaker character varying(10),
    utterance_id integer,
    surface text,
    eng text
);
~~~

Run *sh_combine_utterances* to fill the **all_utterances** table with the data from all the **_cgutterances** tables.

We can now access the context of all words, mutated and unmutated, in Siarad by using:
~~~sql
select t.file, t.utterance_id, t.speaker, t.surface, t.fixed as pos, t.mutation, u.surface, u.eng from all_words t join all_utterances u on t.utterance_id=u.utterance_id and t.file=u.file order by file, t.mutation;
~~~
This takes around 20s to run, so for ease of access the output is provided in *utterances_context.tsv* (67Mb).

To access mutated word context only, for a given file, speaker and mutated word:
~~~sql
select t.file, t.utterance_id, t.speaker, t.surface, t.fixed as pos, t.mutation, u.surface, u.eng from all_mutated t join all_utterances u on t.utterance_id=u.utterance_id and t.file=u.file where t.file='davies10' and t.speaker='CLE' and t.surface='fynd' order by t.mutation, utterance_id;
~~~

To access word context for a given word, mutated or unmutated, for a given file or speaker:
~~~sql
select t.file, t.utterance_id, t.speaker, t.surface, t.fixed as pos, t.mutation, u.surface, u.eng from all_words t join all_utterances u on t.utterance_id=u.utterance_id and t.file=u.file where t.file='davies10' and t.speaker='CLE' and t.surface~'[mf]ynd' order by t.mutation, utterance_id;
~~~

##Sociolinguistic data

Download the questionnaire data from [BangorTalk](bangortalk.org.uk) (*questionnaire/bangortalk.xlsx*).  Extract key fields:

- speaker ID (speaker)
- speaker gender (sex) - Q1
- age
- level of education (educ) - Q5
- Welsh since (welsince) - Q6
- English since (engsince) - Q7
- I keep languages separate (separate) - Q19
- social network mainly Welsh or English (network) - Q15

Two further fields were generated by deduction from "welsince" and "engsince" (see: Deuchar, Donnelly and Piercy (2016). '*Mae pobl monolingual yn minority*': Factors favouring the production of code switching by Welsh-English bilingual speakers. In: *Sociolinguistics in Wales* (ed. Durham and Morris). Palgrave Macmillan):

- first language acquired (lang1): if the entry for "welsince" was less than that for "engsince", L1 was Welsh; if the converse, L1 was English; if both were equal, language acquisition was simultaneous.
- second language acquired (lang2): comparing "welsince" and "engsince", a pair of entries {1,1} signified simultaneous acquisition from birth (SIMULT), {1,2} (or {2,1}) signified that L2 was acquired by age 4 (EARSUC), {1,3} that L2 was acquired in primary school (PRISUC), {1,4} that L2 was acquired in secondary school (SECSUC), and {1,5} that L2 was acquired in adulthood.

Some factors have missing values:

- welsince: ARD
- network: CHR, ISL
- lang1: ARD
- lang2: ARD, EVA, GLA

Replace the missing observation (-99, FALSE) by a blank, to give *questionnaire/siaradq_extract.tsv*.  Note that EVA and GLA have Dutch as their first language, so they will need to be omitted from some of the statistical analysis.

Use *questionnaire/siaradq_extract.tsv* to create the **siaradq** table:
~~~sql
create table siaradq (
    id serial primary key,
    speaker character varying(5),
    sex character varying(2),
    age integer,
    educ integer,
    welsince integer,
    engsince integer,
    separate integer,
    network integer,
    lang1 integer,
    lang2 character varying(10),
    mutrate numeric,
    sm numeric,
    nm numeric,
    am numeric,
    h numeric,
    rno integer,
    cym numeric,
    indet numeric,
    eng numeric
);
~~~

The columns from "mutrate" on will hold data on each speaker's mutation rates and language usage, to be added below.

~~~bash
\copy amash (id, speaker, sex, age, educ, welsince, engsince, lang1, lang2, separate, network) from 'questionnaire/siaradq_extract.tsv' delimiter E'\t' csv header;
~~~

Recode the data so that the codes are linear (e.g. so that Welsh -> English lies on a spectrum, i.e. Welsh=1, both=2, English=3, as opposed to the current questionnaire coding of 0=both, 1= Welsh, 2=English).

| variable | comment | recode=[original code=]meaning |
| --- | --- | --- |
| sex | recode | 1=F=female, 2=M=male |
| age |  no recoding required | N=N |
| educ |  no recoding required | 1=none of the following, 2=GCSE-level, 3=A-level, 4=graduate, 5=postgraduate |
| welsince | no recoding required | 1=<2yo, 2=<4yo, 3=primary, 4=secondary, 5=adult |
| engsince | no recoding required | 1=<2yo, 2=<4yo, 3=primary, 4=secondary, 5=adult |
| separate | no recoding required | 1=strongly disagree, 2=disagree, 3=neither, 4=agree, 5=strongly agree |
| network | recode | 1=1=mainly Welsh, 2=0=both, 3=2=mainly English |
| lang1 | recode | 1=1=Welsh, 2=0=simultaneous, 3=2=English, 4=3=Dutch |
| lang2 | recode | 1=SIMULT=simultaneous, 2=EARSUC=L2 by 4yo, 3=PRISUC=L2 in primary, 4=SECSUC=L2 in secondary, 5=ADUSUC=L2 as adult |

Transfer the recoding to **siaradq** using queries like:
~~~sql
update siaradq set sex= 
    case
        when sex='F' then 1
        when sex='M' then 2
    end;
~~~
~~~sql
alter table siaradq alter column "sex" type integer using sex::integer;
~~~
~~~sql
update siaradq set lang2= 
    case
        when lang2='SIMULT' then 1
        when lang2='EARSUC' then 2
        when lang2='PRISUC' then 3
        when lang2='SECSUC' then 4
        when lang2='ADUSUC' then 5
    end;
~~~
~~~sql
alter table siaradq alter column "lang2" type integer using lang2::integer;
~~~
~~~sql
update siaradq set lang1= 
    case
        when lang1=0 then 2
        when lang1=1 then 1
        when lang1=2 then 3
        when lang1=3 then 4
    end;
~~~

Fill the empty columns in the **siaradq** table with data on each speaker's mutation rates and language usage:
~~~bash
php get_percent_spkr.php
~~~

Note that the final version of the **siaradq** table (*siaradq.tsv*) has a slightly different column order compared to the above, and also contains a running number ("rno") column which is useful in some statistics plots. 

Review overall mutation rate by speaker (*mutation_rate_by_speaker.tsv*):
~~~sql
select speaker, mutrate from siaradq order by mutrate;
~~~

Review initial letter by frequency (*initlet_freq.tsv*):
~~~sql
select letter, count(*) from all_words where langid!='eng' and letter!='' group by letter order by count desc;
~~~

The most frequent words by mutateable letter are in *letter_mutation.pdf*.